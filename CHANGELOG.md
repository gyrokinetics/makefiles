## 8.2.1 (2024-07-12)

* No changes

## 8.2.0 (2024-07-09)

* Minor/nec inlining [PR #81 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/gs2/pull-requests/81)
* Set fft_inc for Archer2 [PR #79 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/79)
* Add FFT_INC to follow recent fft update [PR #80 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/gs2/pull-requests/80)
* Add ANSI_CPP preprocessor flag to avoid error [PR #78 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/gs2/pull-requests/78)
* Bugfix/fix nec options [PR #77 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/gs2/pull-requests/77)
* Make sure cray uses F200x intrinsics [PR #76 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/76)
* Feature/compiler option variables [PR #75 | Peter Hill](https://bitbucket.org/gyrokinetics/gs2/pull-requests/75)
* Remove executable permissions [PR #73 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/73)
* Minor/remove ancient compiler version checks [PR #74 | Peter Hill](https://bitbucket.org/gyrokinetics/gs2/pull-requests/74)
* Differentiate identification of ifx from ifort [PR #72 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/72)
* add Makefile for traverse (Princeton) [PR #71 | Noah Mandell](https://bitbucket.org/gyrokinetics/gs2/pull-requests/71)
* add Makefile for perlmutter (NERSC) [PR #70 | Noah Mandell](https://bitbucket.org/gyrokinetics/gs2/pull-requests/70)
* Remove pathscale compiler [PR #69 | Peter Hill](https://bitbucket.org/gyrokinetics/gs2/pull-requests/69)
* Minor/remove duplicate j flag in cray [PR #68 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/68)
* Add an initial makefile for IFX compiler family. [PR #67 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/67)
* Update Viking makefile for Viking2 [PR #66 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/66)
* Remove defunct, decommissioned, and otherwise dead machines [PR #57 | Peter Hill](https://bitbucket.org/gyrokinetics/gs2/pull-requests/57)
* Fix compilation errors and add macro for optimization [PR #64 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/gs2/pull-requests/64)
* Add option to enhance vectorization [PR #60 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/gs2/pull-requests/60)

## 8.1.2 (2022-07-06)

* Makefile.fedora edited to avoid variable name clash [PR #59 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/59)

## 8.1.1 (2022-01-14)

* Fix gnu-gfortran regex for macports [PR #55 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/gs2/pull-requests/55)
* Replace spaces with tab [PR #56 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/gs2/pull-requests/56)


## 8.1.0 (2021-12-10)

* Add NEC specific clean target clean_nec [PR #51 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/gs2/pull-requests/51)
* Bugfix/fix complaints about bracket in intel makefile [PR #54 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/54)
* Fix gnu-gfortran regex for major version >= 11 [PR #53 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/53)
* Add Makefile.marenostrum [PR #52 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/52)
* Enforce using F2008 intrinsics for gfortran and intel>15 [PR #39 | Peter Hill](https://bitbucket.org/gyrokinetics/gs2/pull-requests/39)
* Minor archer2 tweaks [PR #49 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/49)
* Fixes moddir flag for intel compiler. [PR #48 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/48)
* Update YPI Server Makefile for Ubuntu 20 [PR #47 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/47)


## 8.0.6 (2021-01-26)

* 8.0.6 RC [PR #46 | Joseph Parker](https://bitbucket.org/gyrokinetics/gs2/pull-requests/46)
* Add Makefiles for NIFS Plasma Simulator [PR #38 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/gs2/pull-requests/38)
* Remove jenkins related makefiles [PR #37 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/37)
* Add flags for module compilation/search directory [PR #40 | Peter Hill](https://bitbucket.org/gyrokinetics/gs2/pull-requests/40)
* Add Makefile for Archer2 (gnu compilers) [PR #41 | Joseph Parker](https://bitbucket.org/gyrokinetics/gs2/pull-requests/41)
* Add missing space [PR #45 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/45)
* Try to automatically fix the argument-mismatch error in gfortran 10 [PR #44 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/44)
* Try to use fixed assignment for variables derived from shell calls. [PR #42 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/42)
* Better detect if we're setting USE_FFT or not [PR #43 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/43)
* Replace MPIF90 with FC [PR #36 | David Dickinson](https://bitbucket.org/gyrokinetics/gs2/pull-requests/36)


## 8.0.5 (2020-08-28)

* Changelog for 8.0.5 [PR #35 | Joseph Parker](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/35)
* Allow COMPILER to be overridden in Makefile.local [PR #18 | Peter Hill](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/18)
* Update MacOSX homebrew makefile for Catalina 10.15.5 [PR #34 | Joseph Parker](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/34)
* Change language setting for non-English environment [PR #33 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/33)


## 8.0.4 (2020-02-07)

(No Makefiles changes)


## 8.0.3 (2020-01-20)

* Changelog for 8.0.3 [PR #32 | Joseph Parker](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/32)
* Update Marconi makefile for Intel 2018 [PR #30 | Joseph Parker](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/30)
* Fix build_gs2 on Archer [PR #31 | Joseph Parker](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/31)
* Update to include correct path for parallel netcdf when using OPENMPI in Fedora30. [PR #29 | Colin Malcolm Roach](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/29)


## 8.0.2 (2019-06-10)

* Update 8.0.2 changelog [PR #28 | Joseph Parker](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/28)
* Work-around for compile errors on JFRS [PR #27 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/27)
* Change cray optimization options [PR #25 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/25)
* Remove unnecesary CPP option for JFRS [PR #26 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/26)


## 8.0.2-RC (2019-04-17)

* Add changelog [PR #24 | Joseph Parker](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/24)
* Add Makefile for Anvil's OSX node [PR #23 | Joseph Parker](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/23)
* Remove dead machines [PR #22 | Peter Hill](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/22)
* Make sure `-DFCOMPILER` is added to macro_defs rather than cppflags [PR #21 | David Dickinson](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/21)
* Added Makefile.lisa [PR #20 | George Wilkie](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/20)
* Fix typo in and add GCC support to Viking Makefile [PR #19 | Stephen Biggs-Fox](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/19)
* Add Makefile for STFC's SCARF cluster [PR #17 | Joseph Parker](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/17)
* Add Makefile for STFC's SCARF cluster [PR #16 | Joseph Parker](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/16)
* Add -ffree-form flag to Jenkins builds [PR #15 | Joseph Parker](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/15)
* Add Jenkins Makefiles [PR #14 | Joseph Parker](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/14)


## 8.0.1 (2019-01-31)

* Add Makefiles changelog [PR #12 | Joseph Parker](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/12)
* Update jfrs Makefile [PR #13 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/13)
* Adds makefile for Viking @ York [PR #9 | David Dickinson](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/9)
* Add variable MPI procs capability for NIFS PS [PR #11 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/11)
* Add setup for testing on rokko (SGI Linux Clustelr @ U-Hyogo) [PR #10 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/10)
* Add explicit -O0 to DEBUG flags [PR #8 | Stephen Biggs-Fox](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/8)
* Add a setup for testing on NIFS Plasma Simlator [PR #7 | Ryusuke Numata](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/7)
* Remove -fno-tree-loop-vectorize flag [PR #6 | Joseph Parker](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/6)
* Add FFTW include directory variable [PR #2 | Stephen Biggs-Fox](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/2)
* Add Makefile for Bitbucket pipeline [PR #4 | Peter Hill](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/4)
* Fixed path for FFTW in Fedora27 [PR #5 | Colin Malcolm Roach](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/5)
* Add gcov flags to gnu-gfortran makefile [PR #3 | Joseph Parker](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/3)
* Add Makefile for bitbucket pipeline [PR #1 | Joseph Parker](https://bitbucket.org/gyrokinetics/makefiles/pull-requests/1)


