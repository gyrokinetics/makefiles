# Add some standard compiler options to `F90FLAGS` etc

ifeq ($(FORTRAN_SPEC),2008)
	F90FLAGS += $(FC_STD_08_FLAG)
endif

ifdef DBLE
ifdef QUAD
	F90FLAGS += $(FC_DEFAULT_QUAD_FLAG)
else
	F90FLAGS += $(FC_DEFAULT_DOUBLE_FLAG)
endif
endif

ifdef STATIC
	LDFLAGS += $(LD_STATIC_FLAG)
endif

ifdef DEBUG
	F90FLAGS += $(FC_DEBUG_FLAG)
	F90OPTFLAGS :=
	COPTFLAGS :=
else
    ifdef OPT
        ifeq ($(OPT),aggressive)
            F90OPTFLAGS += $(OPT_AGGRESSIVE_FLAG)
            COPTFLAGS += $(OPT_AGGRESSIVE_FLAG)
        else
            F90OPTFLAGS += $(OPT_FLAG)
            COPTFLAGS += $(OPT_FLAG)
        endif
    endif
endif

ifdef USE_OPENMP
    F90FLAGS += $(OPENMP_FLAG)
    CFLAGS += $(OPENMP_FLAG)
endif

ifdef ONE_STEP_PP
	F90FLAGS += $(FC_PREPROCESS_FLAG) $(FC_FREE_FORM_FLAG)
endif
