# Compiler: NEC nfort/ncc
# Compiler options are apparently the same as gfortran:
# https://www.nec.com/en/global/solutions/hpc/sx/tools.html

FC = nfort
MPIFC = mpinfort
CC = ncc
MPICC =  mpincc

MACRO_DEFS += -DFCOMPILER=_NEC_ -DNO_SIZEOF -DENHANCE_NEC_VECTORIZATION
# Fortran specific flags
MODDIR_FLAG := -J 
FC_FIXED_FORM_FLAG := -ffixed-form
FC_FREE_FORM_FLAG := -ffree-form
FC_PREPROCESS_FLAG := -fpp -DANSI_CPP
FC_NOPREPROCESS_FLAG := -nofpp
FC_STD_08_FLAG := -std=f2008
FC_STD_18_FLAG := -std=f2018
FC_NATIVE_OPT_FLAG := -march=native
FC_DEFAULT_DOUBLE_FLAG := -fdefault-real=8
FC_DEFAULT_QUAD_FLAG := -fdefault-real=8 -fdefault-double=16
FC_DEBUG_FLAG := -O0 -g -Wall -fbounds-check -report-all -traceback -g

# General compiler flags
OPENMP_FLAG := -mparallel -fopenmp
OPT_FLAG := -O3
OPT_AGGRESSIVE_FLAG := -O4
LTO_FLAG := -flto
LD_STATIC_FLAG := -static

include enable_features.make

LDFLAGS += -shared-mpi

## options for inlining to enhance vectorization
## this may take extremely long time
ifdef OPT
    ifeq ($(OPT),aggressive)
	F90FLAGS += -finline-functions -finline-max-function-size=300 -finline-max-depth=3
	ifeq ($(findstring gs2,$(GK_PROJECT)),gs2)
	    # no-inline is necessary to avoid SIGABRT
	    # nfort: /opt/nec/ve/nfort/5.0.1/libexec/fcom is abnormally terminated by SIGABRT
	    # F90FLAGS += -finline-directory=$(GENERATED_SRC_DIR):$(SRCDIR) -fno-inline-file=$(SRCDIR)/array_utils.fpp
	    # F90FLAGS += -finline-directory=$(SRCDIR):$(UTILS):$(NEASY):$(GEO)
	    F90FLAGS += -finline-file=$(SRCDIR)/gs2_layouts.fpp
	    F90FLAGS += -finline-file=$(UTILS)/spfunc.fpp
	    F90FLAGS += -finline-file=$(SRCDIR)/le_grids.f90
	endif
	ifeq ($(findstring agk,$(GK_PROJECT)),agk)
	    F90FLAGS += -finline-directory=.:utils
	endif
    endif
endif

### use ASL for special functions
ifdef USE_MPI
	ifdef USE_OPENMP
		LIBS += -L$(ASL_HOME)/lib -lasl_mpi_openmp
	else
		LIBS += -L$(ASL_HOME)/lib -lasl_mpi_sequential
	endif
else
	ifdef USE_OPENMP
		LIBS += -L$(ASL_HOME)/lib -lasl_openmp
	else
		LIBS += -L$(ASL_HOME)/lib -lasl_sequential
	endif
endif

### -proginf is default
#ifeq ($(findstring proginf,$(PROF)),proginf)
#	F90FLAGS += -proginf
#endif
ifeq ($(findstring ftrace,$(PROF)),ftrace)
	F90FLAGS += -ftrace
endif

clean: clean_nec

clean_nec:
	$(call messageRm,"NEC compiler listing")
	-$(QUIETSYM)rm -rf *.L
